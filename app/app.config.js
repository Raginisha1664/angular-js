'use strict';

angular.
  module('job').
  config(['$locationProvider' ,'$routeProvider',
    function config($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');

      $routeProvider.
        when('/jobs', {
          template: '<job-list></job-list>'
        }).
       
        otherwise('/jobs');
    }
  ]);
