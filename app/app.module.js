'use strict';

// Define the `job` module
angular.module('job', [
  'ngAnimate',
  'ngRoute',
  'core',
  'jobList'
]);
